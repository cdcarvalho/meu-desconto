module.exports = {
    dialect: 'postgres',
    host: 'localhost',
    username: 'postgres',
    password: '123@qwe',
    database: 'meu_desconto',
    define: {
        timestamps: true,
        underscored: true,
    },
};