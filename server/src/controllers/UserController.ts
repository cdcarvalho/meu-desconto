import { Request, Response } from 'express';
import cryptoUtil from '../util/cryptoUtil';
const User = require('../models/User');

class UserController {
    async index(request: Request, response: Response) {
        try {
            const users = await User.findAll({
                order: ['name']
            });
            return response.json(users);
        } catch (error) {
            response.json({
                error: true,
                message: error.message
            });
        }
    }

    async findById(request: Request, response: Response) {
        const { id } = request.params;
        const user = await User.findByPk(id);
        return response.json(user);
    }

    async create(request: Request, response: Response) {
        try {
            const { name,
                email,
                username,
                password,
                admin,
                active } = request.body;

            const user = await User.create({
                name,
                email,
                username,
                password: cryptoUtil(password),
                admin,
                active
            });

            return response.json({
                User: user,
                mensagem: "Usuário Inserido com sucesso!"
            });
        } catch (error) {
            response.json({
                error: true,
                message: error.message
            });
        }
    }

    async deleteById(request: Request, response: Response) {
        try {
            const { id } = request.params;
            await User.destroy({
                where: {
                    id: id
                }
            });
            return response.json("Registro removido com sucesso!");
        } catch (error) {
            response.json({
                error: true,
                message: error.message
            });
        }
    }

    async update(request: Request, response: Response) {
        try {
            const { id } = request.params;
            const { name,
                email,
                username,
                password,
                admin,
                active } = request.body;

            const user_id = await User.update(
                {
                    name: name,
                    email: email,
                    username: username,
                    password: cryptoUtil(password),
                    admin: admin,
                    active: active
                },
                { where: { id: id } }
            );
            return response.json({
                user_id,
                mensagem: "Registro Alterado com sucesso!"
            });
        } catch (error) {
            response.json({
                error: true,
                message: error.message
            });
        }
    }
}


export default UserController;