import { Request, Response } from 'express';
const Store = require('../models/Store');

class StoreController {
    async index(request: Request, response: Response) {
        try {
            const stores = await Store.findAll({
                order: ['name']
            });
            return response.json(stores);
        } catch (error) {
            response.json({
                error: true,
                message: error.message
            });
        }
    }

    async findById(request: Request, response: Response) {
        const { id } = request.params;
        const store = await Store.findByPk(id);
        return response.json(store);
    }

    async create(request: Request, response: Response) {
        try {
            const { name,
                cpf_cnpj,
                email,
                phone,
                whatsapp,
                facebook,
                instragram,
                responsible } = request.body;

            const store = await Store.create({
                name,
                cpf_cnpj,
                email,
                phone,
                whatsapp,
                facebook,
                instragram,
                responsible,
            });

            return response.json({
                Store: store,
                mensagem: "Loja cadastrada com sucesso!"
            });
        } catch (error) {
            response.json({
                error: true,
                message: error.message
            });
        }
    }

    async deleteById(request: Request, response: Response) {
        try {
            const { id } = request.params;
            await Store.destroy({
                where: {
                    id: id
                }
            });
            return response.json("Registro removido com sucesso!");
        } catch (error) {
            response.json({
                error: true,
                message: error.message
            });
        }
    }

    async update(request: Request, response: Response) {
        try {
            const { id } = request.params;
            const { name,
                cpf_cnpj,
                email,
                phone,
                whatsapp,
                facebook,
                instragram,
                responsible } = request.body;

            const store_id = await Store.update(
                {
                    name: name,
                    cpf_cnpj: cpf_cnpj,
                    email: email,
                    phone: phone,
                    whatsapp: whatsapp,
                    facebook: facebook,
                    instragram: instragram,
                    responsible: responsible
                },
                { where: { id: id } }
            );
            return response.json({
                store_id,
                mensagem: "Registro Alterado com sucesso!"
            });
        } catch (error) {
            response.json({
                error: true,
                message: error.message
            });
        }
    }
}


export default StoreController;