import { Request, Response } from 'express';
const Category = require('../models/Category');

class CategoryController {
    async index(request: Request, response: Response) {
        try {
            const categories = await Category.findAll();
            return response.json(categories);
        } catch (error) {
            response.json({
                error: true,
                message: error.message
            });
        }
    }

    async findById(request: Request, response: Response) {
        const { id } = request.params;
        const category = await Category.findByPk(id);
        if (category == null) {
            return response.json("Nenhum registro encontrado.");
        }
        return response.json(category);
    }

    async create(request: Request, response: Response) {
        try {
            const { description } = request.body;
            const category_id = await Category.create({ description });

            return response.json({
                id: category_id,
                mensagem: "Inserido com sucesso!"
            });
        } catch (error) {
            response.json({
                error: true,
                message: error.message
            });
        }
    }

    async deleteById(request: Request, response: Response) {
        try {
            const { id } = request.params;
            await Category.destroy({
                where: {
                    id: id
                }
            });
            return response.json("Registro removido com sucesso!");
        } catch (error) {
            response.json({
                error: true,
                message: error.message
            });
        }
    }

    async update(request: Request, response: Response) {
        try {
            const { id } = request.params;
            const { description } = request.body;
            const category_id = await Category.update(
                { description: description },
                { where: { id: id } }
            );
            return response.json({
                category_id,
                mensagem: "Registro Alterado com sucesso!"
            });
        } catch (error) {
            response.json({
                error: true,
                message: error.message
            });
        }
    }
}


export default CategoryController;