import { Request, Response } from 'express';
import cryptoUtil from '../util/cryptoUtil';
import isEmpty from './../util/arrayUtil';
import idByJson from '../util/idByJsonUtil';
import adminByJson from '../util/adminByJsonUtil';
import jwt from 'jsonwebtoken'
import { LocalStorage } from "node-localstorage";

global.localStorage = new LocalStorage('./scratch');

const User = require('../models/User');
const auth = require('./../config/secret');
const httpEnum = require('./../enum/Ehttp');
const secretHash = require('./../config/secret');


class LoginController {

    async authenticate(request: Request, response: Response) {
        const [, hash] = request.headers.authorization?.split(' ')
        const [username, password] = Buffer.from(hash, 'base64').toString().split(":");

        try {
            const user = await User.findAll(
                {
                    where:
                    {
                        username: username,
                        password: cryptoUtil(password)
                    }
                });

            if (isEmpty(user)) {
                response.sendStatus(httpEnum.httpStatusCode.UNAUTHORIZED);
            }
            
            const token = jwt.sign({ id: idByJson(user) }, auth.SECRET, { expiresIn: 7200 });

            return response.json({ 'token': token, 'isAdmin': adminByJson(user) });
        } catch (error) {
            response.sendStatus(httpEnum.httpStatusCode.SERVER_ERROR);
        }
    }

    async me(request: Request, response: Response) {
        try {
            const { id } = request.params;
            const user = await User.findByPk(id);
            return response.json(user);
        } catch (error) {
            response.json(error);
        }
    }

    async authenticated(request: Request, response: Response) {
        const authHeader = request.headers.authorization;

        if(!authHeader) {
            return response.status(httpEnum.httpStatusCode.UNAUTHORIZED).json({ message: 'Token is required!'});
        }
        
        const [, token] = authHeader.split(' ');
    
        try {
            await jwt.verify(token, secretHash.SECRET);
            return response.send(true)
        } catch (error) {
            return response.send(false)
        }
    }
}

export default LoginController;