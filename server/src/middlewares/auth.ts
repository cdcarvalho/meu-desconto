import { Request, Response, NextFunction } from 'express';
import jwt from 'jsonwebtoken'
const httpEnum = require('./../enum/Ehttp');
const secretHash = require('./../config/secret');

export const auth = async(request: Request, response: Response, next: NextFunction) => {
    
    const authHeader = request.headers.authorization;
    
    if(!authHeader) {
        return response.status(httpEnum.httpStatusCode.UNAUTHORIZED).json({ message: 'Token is required!'});
    }
    
    const [, token] = authHeader.split(' ');

    try {
        await jwt.verify(token, secretHash.SECRET);
        next()   
    } catch (error) {
        return response.status(httpEnum.httpStatusCode.UNAUTHORIZED).json({ message: 'Token invalid!'});
    }
}