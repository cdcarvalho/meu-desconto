import * as express from 'express';
import CategoryController from './controllers/CategoryController';
import UserController from './controllers/UserController';
import LoginController from './controllers/LoginController';
import StoreController from './controllers/StoreController';
import { auth } from './middlewares/auth';

const routes = express.Router();
const categoryController = new CategoryController();
const userController = new UserController();
const loginController = new LoginController();
const storeController = new StoreController();

//Login
routes.get('/authenticate', loginController.authenticate);
routes.get('/authenticated', loginController.authenticated);
routes.get('/me', loginController.me);

//routes with user logged
routes.use(auth);

//Category
routes.get('/categories', categoryController.index);
routes.get('/category/:id', categoryController.findById);
routes.post('/category/create', categoryController.create);
routes.delete('/category/delete/:id', categoryController.deleteById);
routes.put('/category/update/:id', categoryController.update);

//User
routes.get('/users', userController.index);
routes.get('/user/:id', userController.findById);
routes.post('/user/create', userController.create);
routes.delete('/user/delete/:id', userController.deleteById);
routes.put('/user/update/:id', userController.update);

//Store
routes.get('/stores', storeController.index);
routes.get('/store/:id', storeController.findById);
routes.post('/store/create', storeController.create);
routes.delete('/store/delete/:id', storeController.deleteById);
routes.put('/store/update/:id', storeController.update);

export default routes;