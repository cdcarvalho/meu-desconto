const Sequelize = require('sequelize');
const dbconfig = require('../config/database');

const Category = require('../models/Category');
const User = require('../models/User');
const Store = require('../models/Store');

const connection = new Sequelize(dbconfig);

Category.init(connection);
User.init(connection);
Store.init(connection);

module.exports = connection;