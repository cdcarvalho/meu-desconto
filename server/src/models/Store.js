const { Model, DataTypes } = require('sequelize');

class Store extends Model {

    static init(sequelize) {
        super.init({
            name: DataTypes.STRING,
            cpf_cnpj: DataTypes.STRING,
            email: DataTypes.STRING,
            phone: DataTypes.STRING,
            whatsapp: DataTypes.STRING,
            facebook: DataTypes.STRING,
            instragram: DataTypes.STRING,
            responsible: DataTypes.STRING
        }, {
            sequelize
        })
    }
}

module.exports = Store;