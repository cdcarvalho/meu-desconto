
export default (array: []) => {
    return Array.isArray(array) && array.length === 0;
}