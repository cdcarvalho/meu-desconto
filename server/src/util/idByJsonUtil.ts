export default (json: string) => {
    var id
    var jsonParse = JSON.parse(JSON.stringify(json, ["id"]))
    for (var i = 0; i < jsonParse.length; i++){
        id = jsonParse[i].id;
        break
    }
    return id;
}