export default (json: string) => {
    var isAdmin
    var jsonParse = JSON.parse(JSON.stringify(json, ["admin"]))
    for (var i = 0; i < jsonParse.length; i++){
        isAdmin = jsonParse[i].admin;
        break
    }
    return isAdmin;
}