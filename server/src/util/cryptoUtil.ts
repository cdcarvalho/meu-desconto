import crypto from 'crypto';


export default (password: string) => {
    return crypto.createHash('md5').update(password).digest('hex');
}