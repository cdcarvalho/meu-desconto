export default class User {
    id: number
    name: string
    email: string
    username: string
    password: string
    admin: boolean
    active: boolean
}
