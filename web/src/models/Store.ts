export default class Store {
    id: number
    name: string
    cpf_cnpj: string
    email: string
    phone: string
    whatsapp: string
    facebook: string
    instragram: string
    responsible: string
}