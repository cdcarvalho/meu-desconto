import React from 'react';
import { Route, BrowserRouter, Switch, Redirect } from 'react-router-dom';
import { isAuthenticated } from './_services/auth';

import Home from './_components/public/home/Home';
import Login from './_components/public/login/Login';
import Dashboard from './_components/admin/dashboard/Dashboard';
import CreateStore from './_components/admin/store/CreateStore';
import EditStore from './_components/admin/store/EditStore';
import ListStore from './_components/admin/store/ListStore';
import ListCategory from './_components/admin/category/ListCategory';
import CreateCategory from './_components/admin/category/CreateCategory';
import EditCategory from './_components/admin/category/EditCategory';
import ListUser from './_components/admin/user/ListUser';
import CreateUser from './_components/admin/user/CreateUser';
import EditUser from './_components/admin/user/EditUser';

const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => (
        isAuthenticated() ? (
            <Component {...props} />
        ) : (
                <Redirect to={{ pathname: '/authenticate', state: { from: props.location } }} />
            )
    )} />
)

const Routes = () => {
    return (
        <BrowserRouter>
            <Switch>
                <Route component={Home} path="/" exact />
                <Route component={Login} path="/authenticate" />
                <PrivateRoute component={Dashboard} path="/dashboard" exact />
                <PrivateRoute component={CreateStore} path="/store/create" />
                <PrivateRoute component={EditStore} path="/store/edit/:id" />
                <PrivateRoute component={ListStore} path="/store/list" />
                <PrivateRoute component={ListCategory} path="/category/list" />
                <PrivateRoute component={CreateCategory} path="/category/create" />
                <PrivateRoute component={EditCategory} path="/category/edit/:id" />
                <PrivateRoute component={ListUser} path="/user/list" />
                <PrivateRoute component={CreateUser} path="/user/create" />
                <PrivateRoute component={EditUser} path="/user/edit/:id" />
            </Switch>
        </BrowserRouter>
    );
}

export default Routes;