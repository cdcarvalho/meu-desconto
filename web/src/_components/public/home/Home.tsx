import React from 'react';
import { useHistory } from 'react-router-dom';
import './home.css';

const Home = () => {
    const history = useHistory();

    function login() {
        history.push('/authenticate');
    };

    return(
        <div id="page-home">
            <div className="content">
                <header>
                    <h1>Meu Desconto</h1>
                </header>

            <main>
                <h1>Seu marketplace de produtos e serviços.</h1>
                <p>Ajudamos pessoas a encontrarem as melhores promoções de forma eficiente.</p>

                <button onClick={login}>
                    <strong>Logar</strong>
                </button>
            </main>
            </div>
        </div>
    );
};

export default Home;