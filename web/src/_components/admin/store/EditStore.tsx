import React, { useState, useEffect, ChangeEvent, FormEvent } from 'react'
import { Form, FormGroup, Label, Input, Button } from 'reactstrap'
import { useHistory, useParams } from 'react-router-dom'
import api from '../../../_services/api'
import { getToken } from '../../../_services/auth'

export const EditStore = () => {
  const history = useHistory()
  const { id } = useParams()

  const [inputData, setInputData] = useState({
    name: '',
    cpf_cnpj: '',
    email: '',
    phone: '',
    whatsapp: '',
    facebook: '',
    instragram: '',
    responsible: ''
  })

  function changeInput(event: ChangeEvent<HTMLInputElement>) {
    const { name, value } = event.target
    setInputData({ ...inputData, [name]: value })
  }

  async function changeForm(event: FormEvent) {
    event.preventDefault()

    const {
      name,
      cpf_cnpj,
      email,
      phone,
      whatsapp,
      facebook,
      instragram,
      responsible
    } = inputData

    const data = {
      name,
      cpf_cnpj,
      email,
      phone,
      whatsapp,
      facebook,
      instragram,
      responsible
    }

    await api.put(`store/update/${id}`, data, {
      headers: {
        Authorization: `Basic ${getToken()}`
      }
    })

    alert('Loja alterada com sucesso!')

    history.goBack()
  }

  useEffect(() => {
    loadStore(id)
  }, [id])

  async function loadStore(id: string) {
    const response = await api.get(`store/${id}`, {
      headers: {
        Authorization: `Basic ${getToken()}`
      }
    })

    const store = {
      name: response.data.name,
      cpf_cnpj: response.data.cpf_cnpj,
      email: response.data.email,
      phone: response.data.phone,
      whatsapp: response.data.whatsapp,
      facebook: response.data.facebook,
      instragram: response.data.instragram,
      responsible: response.data.responsible
    }

    setInputData(store)
  }

  function cancel() {
    history.goBack()
  }

  return (
    <div id="page-create-store" className="body_form">
      <Form onSubmit={changeForm}>
        <FormGroup>
          <Label className="strong">Nome:</Label>
          <Input
            id="name"
            type="text"
            name="name"
            placeholder="Nome"
            value={inputData.name}
            required
            onChange={changeInput}></Input>

          <Label className="strong">Email:</Label>
          <Input
            id="cpf_cnpj"
            type="text"
            name="cpf_cnpj"
            placeholder="CPF/CNPJ"
            value={inputData.cpf_cnpj}
            onChange={changeInput}></Input>

          <Label className="strong">Email:</Label>
          <Input
            id="email"
            type="email"
            name="email"
            placeholder="Email"
            value={inputData.email}
            onChange={changeInput}></Input>

          <Label className="strong">Telefone:</Label>
          <Input
            id="phone"
            type="text"
            name="phone"
            placeholder="Telefone"
            maxLength={11}
            value={inputData.phone}
            onChange={changeInput}></Input>

          <Label className="strong">Whatsapp:</Label>
          <Input
            id="whatsapp"
            type="text"
            name="whatsapp"
            placeholder="Whatsapp"
            value={inputData.whatsapp}
            required
            onChange={changeInput}></Input>

          <Label className="strong">Facebook:</Label>
          <Input
            id="facebook"
            type="text"
            name="facebook"
            placeholder="facebook"
            value={inputData.facebook}
            required
            onChange={changeInput}></Input>

          <Label className="strong">Instragram:</Label>
          <Input
            id="instragram"
            type="text"
            name="instragram"
            placeholder="Instragram"
            value={inputData.instragram}
            required
            onChange={changeInput}></Input>

          <Label className="strong">Responsável:</Label>
          <Input
            id="responsible"
            type="text"
            name="responsible"
            placeholder="Responsável"
            value={inputData.facebook}
            required
            onChange={changeInput}></Input>

        </FormGroup>

        <Button id="cancelar" onClick={cancel}>Cancelar</Button>
        <Button type="submit">Alterar</Button>
      </Form>
    </div>
  )
}
export default EditStore
