import React, { useEffect, useState, ChangeEvent, FormEvent } from 'react';
import { Form, FormGroup, Label, Input, Button, Container } from 'reactstrap';
import { Map, TileLayer, Marker } from 'react-leaflet'
import { useHistory } from 'react-router-dom';
import api from '../../../_services/api';
import axios from 'axios';
import { LeafletMouseEvent } from 'leaflet';
import UF from '../../../models/UF'
import City from '../../../models/City'
import { getToken } from '../../../_services/auth';

import './store.css'

const CreateStore = () => {

    const [inputData, setInputData] = useState({
        name: '',
        cpf_cnpj: '',
        email: '',
        phone: '',
        whatsapp: '',
        facebook: '',
        instragram: '',
        responsible: ''
    });

    const [ufs, setUfs] = useState<string[]>([]);
    const [cities, setCities] = useState<string[]>([]);
    const [initialPositionMap, setInitialPositionMap] = useState<[number, number]>([0, 0]);

    const [selectedUf, setSelectedUf] = useState('0');
    const [selectedCity, setSelectedCity] = useState('0');
    const [selectedPositionMap, setSelectedPositionMap] = useState<[number, number]>([0, 0]);
    const history = useHistory();

    useEffect(() => {
        navigator.geolocation.getCurrentPosition(position => {
            const { latitude, longitude } = position.coords;
            setInitialPositionMap([latitude, longitude]);
            setSelectedPositionMap([latitude, longitude]);
        });
    }, []);

    useEffect(() => {
        axios.get<UF[]>('https://servicodados.ibge.gov.br/api/v1/localidades/estados').then(ufs => {
            const ufsIbge = ufs.data.map(uf => uf.sigla);
            setUfs(ufsIbge);
        });
    }, []);

    useEffect(() => {
        axios.get<City[]>(`https://servicodados.ibge.gov.br/api/v1/localidades/estados/${selectedUf}/municipios`)
            .then(cities => {
                const citiesIbge = cities.data.map(city => city.nome);
                setCities(citiesIbge);
            });
    }, [selectedUf]);

    function changeUf(event: ChangeEvent<HTMLSelectElement>) {
        const uf = event.target.value;
        setSelectedUf(uf);
    }

    function changeCity(event: ChangeEvent<HTMLSelectElement>) {
        const city = event.target.value;
        setSelectedCity(city);
    }

    function changeMapClick(event: LeafletMouseEvent) {
        setSelectedPositionMap([
            event.latlng.lat,
            event.latlng.lng
        ]);
    }

    function changeInput(event: ChangeEvent<HTMLInputElement>) {
        const { name, value } = event.target;
        setInputData({ ...inputData, [name]: value });
    }

    async function changeForm(event: FormEvent) {
        event.preventDefault();

        const { name,
            cpf_cnpj,
            email,
            phone,
            whatsapp,
            facebook,
            instragram,
            responsible } = inputData;

        const uf = selectedUf;
        const city = selectedCity
        const [latitude, longitude] = selectedPositionMap;

        const data = {
            name,
            cpf_cnpj,
            email,
            phone,
            whatsapp,
            facebook,
            instragram,
            responsible
        };

        await api.post('store/create', data, {
            headers: {
                'Authorization': `Basic ${getToken()}`
            },
        });


        alert('Loja Cadastrada com Sucesso!')

        history.goBack();
    }

    function cancel() {
        history.goBack();
    }


    return (
        <Container>
            <div id="page-create-store" className="body_form">
                <Form onSubmit={changeForm}>
                    <FormGroup>

                        <Label className="strong">Nome:</Label>
                        <Input
                            id="name"
                            type="text"
                            name="name"
                            placeholder="Nome"
                            value={inputData.name}
                            required
                            onChange={changeInput}></Input>

                        <Label className="strong">Email:</Label>
                        <Input
                            id="cpf_cnpj"
                            type="text"
                            name="cpf_cnpj"
                            placeholder="CPF/CNPJ"
                            value={inputData.cpf_cnpj}
                            onChange={changeInput}></Input>

                        <Label className="strong">Email:</Label>
                        <Input
                            id="email"
                            type="email"
                            name="email"
                            placeholder="Email"
                            value={inputData.email}
                            onChange={changeInput}></Input>

                        <Label className="strong">Telefone:</Label>
                        <Input
                            id="phone"
                            type="text"
                            name="phone"
                            placeholder="Telefone"
                            maxLength={11}
                            value={inputData.phone}
                            onChange={changeInput}></Input>

                        <Label className="strong">Whatsapp:</Label>
                        <Input
                            id="whatsapp"
                            type="text"
                            name="whatsapp"
                            placeholder="Whatsapp"
                            value={inputData.whatsapp}
                            required
                            onChange={changeInput}></Input>

                        <Label className="strong">Facebook:</Label>
                        <Input
                            id="facebook"
                            type="text"
                            name="facebook"
                            placeholder="facebook"
                            value={inputData.facebook}
                            required
                            onChange={changeInput}></Input>

                        <Label className="strong">Instragram:</Label>
                        <Input
                            id="instragram"
                            type="text"
                            name="instragram"
                            placeholder="Instragram"
                            value={inputData.instragram}
                            required
                            onChange={changeInput}></Input>

                        <Label className="strong">Responsável:</Label>
                        <Input
                            id="responsible"
                            type="text"
                            name="responsible"
                            placeholder="Responsável"
                            value={inputData.responsible}
                            required
                            onChange={changeInput}></Input>


                        <fieldset>
                            <legend>
                                <h2>Endereço</h2>
                                <span>Selecione o endereço no mapa</span>
                            </legend>

                            <Map center={initialPositionMap} zoom={15} onClick={changeMapClick}>
                                <TileLayer
                                    attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                                />
                                <Marker position={selectedPositionMap} />
                            </Map>

                            <div className="field-group">
                                <div className="field">
                                    <label htmlFor="uf">Estado (UF)</label>
                                    <select name="uf" id="uf" value={selectedUf} onChange={changeUf}>
                                        <option value="0">Selecione um Estado</option>
                                        {
                                            ufs.map(uf => (
                                                <option key={uf} value={uf}>{uf}</option>
                                            ))
                                        }
                                    </select>
                                </div>

                                <div className="field">
                                    <label htmlFor="city">Cidade</label>
                                    <select name="city" id="city" value={selectedCity} onChange={changeCity}>
                                        <option value="0">Selecione uma Cidade</option>
                                        {
                                            cities.map(city => (
                                                <option key={city} value={city}>{city}</option>
                                            ))
                                        }
                                    </select>
                                </div>
                            </div>
                        </fieldset>

                    </FormGroup>

                    <Button id="cancelar" onClick={cancel}>Cancelar</Button>
                    <Button type="submit">Salvar</Button>
                </Form>
            </div>
        </Container>
    );
};

export default CreateStore;