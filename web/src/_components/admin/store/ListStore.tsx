import React, { useEffect, useState } from 'react'
import { Table, Button, Navbar, Nav, NavItem, NavbarBrand } from 'reactstrap'
import { Link } from 'react-router-dom'
import { FiArrowLeft } from 'react-icons/fi'

import './store.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import Store from '../../../models/Store'
import StoreService from '../../../_services/store/storeService';
const ListStore = () => {
  const [stores, setStores] = useState<Store[]>([])
  const storeService = new StoreService();

  useEffect(() => {
    let findStore = true;
    try {
      storeService.getStores().then(stores => {
        if (findStore) {
          setStores(stores);
        }
      }).catch((error: string) => {
        alert(error)
      });
    } catch (error) {
      alert(error);
    }

    return function cleanup() {
      findStore = false
    }

  }, [storeService])

  function getStores() {
    try {
      storeService.getStores().then(stores => {
        setStores(stores);
      });
    } catch (error) {
      alert(error);
    }
  }

  const deleteBy = async (event: any, id: number) => {
    event.persist()
    try {
      storeService.delete(id).then(() => {
        getStores()
      });
    } catch (error) {
      alert(error);
    }
  }

  return (
    <div className="body">

      <Navbar color="dark" dark>
        <NavbarBrand href="/store/list">Lojas</NavbarBrand>
        <Nav>
          <NavItem>
            <Link to="/store/create" className="btn btn-primary link-menu">Novo</Link>
          </NavItem>
        </Nav>
      </Navbar>

      <Table bordered>
        <thead>
          <tr>
            <th className="cabecalho">ID</th>
            <th className="cabecalho">Nome</th>
            <th className="cabecalho">CPF/CNPJ</th>
            <th className="cabecalho">Email</th>
            <th className="cabecalho">Telefone</th>
            <th className="cabecalho">Whatsapp</th>
            <th className="cabecalho">Facebook</th>
            <th className="cabecalho">Instragram</th>
            <th className="cabecalho">Responsável</th>
            <th className="cabecalho">Ação</th>
          </tr>
        </thead>
        {stores.map(store => (
          <tbody key={store.id}>
            <tr>
              <th scope="row" >{store.id}</th>
              <td className="row_center" style={{ width: '20%' }}>{store.name}</td>
              <td className="row_center" style={{ width: '15%' }}>{store.cpf_cnpj}</td>
              <td className="row_center" style={{ width: '25%' }}>{store.email}</td>
              <td className="row_center" style={{ width: '10%' }}>{store.phone}</td>
              <td className="row_center" style={{ width: '10%' }}>{store.whatsapp}</td>
              <td className="row_center" style={{ width: '20%' }}>{store.facebook}</td>
              <td className="row_center" style={{ width: '15%' }}>{store.instragram}</td>
              <td className="row_center" style={{ width: '15%' }}>{store.responsible}</td>

              <td><div className="ml-auto">
                <Link className="btn btn-warning mr-1"
                  to={`/store/edit/${store.id}`}>Editar</Link>
                <Button color="danger"
                  onClick={e => deleteBy(e, store.id)}>Remover</Button>
              </div></td>
            </tr>
          </tbody>
        ))}
      </Table>

      <footer>
        <Link to="/dashboard">
          <FiArrowLeft />
                Voltar
        </Link>
      </footer>
    </div>
  )
}

export default ListStore
