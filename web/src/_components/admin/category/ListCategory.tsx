import React, { useEffect, useState } from 'react';
import { Table, Button, Navbar, Nav, NavItem, NavbarBrand } from 'reactstrap';
import { Link } from 'react-router-dom';
import { FiArrowLeft } from 'react-icons/fi';
import Category from '../../../models/Category'
import CategoryService from '../../../_services/category/categoryService';


import './category.css'
import 'bootstrap/dist/css/bootstrap.min.css'

const ListCategory = () => {
    const [categories, setCategories] = useState<Category[]>([]);
    const categoryService = new CategoryService();

    useEffect(() => {
        let findCategory = true;
        try {
            categoryService.getCategories().then(categories => {
                if (findCategory) {
                    setCategories(categories);
                }
            }).catch((error: string) => {
                alert(error);
            });
        } catch (error) {
            alert(error);
        }

        return function cleanup() {
            findCategory = false
        }
    }, [categoryService])

    function getCaterories() {
        try {
            categoryService.getCategories().then(categories => {
                setCategories(categories);
            });
        } catch (error) {
            alert(error);
        }
    }

    const deleteBy = async (event: any, id: number) => {
        event.persist();
        try {
            categoryService.delete(id).then(() => {
                getCaterories()
            });
        } catch (error) {
            alert(error);
        }
    };

    return (
        <div className="body">

            <Navbar color="dark" dark>
                <NavbarBrand href="/category/list">Categorias</NavbarBrand>
                <Nav>
                    <NavItem>
                        <Link to="/category/create" className="btn btn-primary link-menu">Novo</Link>
                    </NavItem>
                </Nav>
            </Navbar>

            <Table bordered>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Descrição</th>
                        <th>Ação</th>
                    </tr>
                </thead>
                {categories.map(category => (
                    <tbody key={category.id}>
                        <tr>
                            <th scope="row" >{category.id}</th>
                            <td style={{ width: "80%" }}>{category.description}</td>
                            <td><div className="ml-auto">
                                <Link className="btn btn-warning mr-1"
                                    to={`/category/edit/${category.id}`}>Editar</Link>
                                <Button color="danger"
                                    onClick={e => deleteBy(e, category.id)}>Remover</Button>
                            </div></td>
                        </tr>
                    </tbody>
                ))}
            </Table>

            <footer>
                <Link to="/dashboard">
                    <FiArrowLeft />
                Voltar
            </Link>
            </footer>
        </div>
    );
};

export default ListCategory;