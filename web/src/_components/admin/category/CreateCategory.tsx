import React, { useState, ChangeEvent, FormEvent } from 'react'
import {Form, FormGroup, Label, Input, Button} from 'reactstrap';
import { useHistory } from 'react-router-dom';
import api from  '../../../_services/api';
import { getToken }  from '../../../_services/auth';

export const CreateCategory = () => {

    const history = useHistory();

    const [inputData, setInputData] = useState({
        description: '',
    });

    function changeInput(event: ChangeEvent<HTMLInputElement>) {
        const { name, value } = event.target;
        setInputData({ ...inputData, [name]: value });
    }

    async function changeForm(event: FormEvent) {
        event.preventDefault();

        const { description } = inputData;
        
        const data = {
            description
        };

       await api.post('category/create', data, {
        headers: {
            'Authorization': `Basic ${getToken()}`
        },
    });

       alert('Categoria Cadastrada com Sucesso!')

       history.goBack();
    }

    function cancel() {
        history.goBack();
    }

    return (
        <div id="page-create-category" className="body">
            <Form onSubmit={changeForm}>
                <FormGroup>
                    <Label className="strong">Descrição:</Label>
                    <Input 
                        id="description" 
                        type="text" 
                        name="description" 
                        placeholder="Digite a Descrição da Categoria"
                        required
                        onChange={changeInput}></Input>
                </FormGroup>

                <Button id="cancelar" onClick={cancel}>Cancelar</Button>
                <Button type="submit">Salvar</Button>
            </Form>
        </div>
    )
}
export default CreateCategory;
