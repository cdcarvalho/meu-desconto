import React, { useState, useEffect, ChangeEvent, FormEvent } from 'react'
import { Form, FormGroup, Label, Input, Button } from 'reactstrap';
import { useHistory, useParams } from 'react-router-dom';
import api from '../../../_services/api';
import { getToken } from '../../../_services/auth';

export const EditCategory = () => {

    const history = useHistory();
    const { id } = useParams();

    const [inputData, setInputData] = useState({
        description: '',
    });

    function changeInput(event: ChangeEvent<HTMLInputElement>) {
        const { name, value } = event.target;
        setInputData({ ...inputData, [name]: value });
    }

    async function changeForm(event: FormEvent) {
        event.preventDefault();

        const { description } = inputData;

        await api.put(`category/update/${id}`, { description: description }, {
            headers: {
                'Authorization': `Basic ${getToken()}`
            },
        });

        alert('Categoria Alterada com Sucesso!')

        history.goBack();
    }

    useEffect(() => {
        loadCatogoty(id);
    }, [id]);


    async function loadCatogoty(id: string) {

        const response = await api.get(`category/${id}`, {
            headers: {
                'Authorization': `Basic ${getToken()}`
            },
        });

        setInputData({
            description: response.data.description
        });
    }

    function cancel() {
        history.goBack();
    }

    return (
        <div id="page-create-category" className="body">
            <Form onSubmit={changeForm}>
                <FormGroup>
                    <Label className="strong">Descrição:</Label>
                    <Input
                        id="name"
                        type="text"
                        name="description"
                        value={inputData.description}
                        placeholder="Digite a Descrição da Categoria"
                        onChange={changeInput}></Input>
                </FormGroup>

                <Button id="cancelar" onClick={cancel}>Cancelar</Button>
                <Button type="submit">Alterar</Button>
            </Form>
        </div>
    )
}
export default EditCategory;
