import React from 'react';
import { Link } from 'react-router-dom';
import { Navbar, Nav, NavItem, NavbarBrand } from 'reactstrap';

import './header.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import { isAdmin } from './../../../_services/auth';


const Header = () => {

    function menuAdmin() {
        return isAdmin() === 'true' ? 'btn btn-primary link-menu' : 'btn btn-primary link-menu disabled'
    }

    function logout() {
        localStorage.clear();
    }

    return (
        <div id="header" className="body">

            <Navbar color="dark" dark>
                <NavbarBrand href="/dashboard">Inicio</NavbarBrand>
                <Nav>
                    <NavItem>
                        <Link to="/category/list" className={menuAdmin()}>Categorias</Link>
                    </NavItem>
                    <NavItem>
                        <Link to="/store/list" className={menuAdmin()}>Lojas</Link>
                    </NavItem>
                    <NavItem>
                        <Link to="/dashboard" className={menuAdmin()}>Produtos</Link>
                    </NavItem>
                    <NavItem>
                        <Link to="/user/list" className={menuAdmin()}>Usuários</Link>
                    </NavItem>
                    <NavItem>
                        <Link to="/authenticate" onClick={() => logout()} className="btn btn-secondary link-menu">Sair</Link>
                    </NavItem>
                </Nav>
            </Navbar>
        </div>
    );
};

export default Header;