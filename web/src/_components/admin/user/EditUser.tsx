import React, { useState, useEffect, ChangeEvent, FormEvent } from 'react'
import { Form, FormGroup, Label, Input, Button } from 'reactstrap'
import { useHistory, useParams } from 'react-router-dom'
import api from '../../../_services/api'
import { getToken } from '../../../_services/auth'

export const EditCategory = () => {
  const history = useHistory()
  const { id } = useParams()

  const [inputData, setInputData] = useState({
    name: '',
    email: '',
    username: '',
    password: '',
    admin: false,
    active: false
  })

  function changeAdmin(checked: boolean) {
    inputData.admin = checked
    setInputData({ ...inputData })
  }

  function changeActive(checked: boolean) {
    inputData.active = checked
    setInputData({ ...inputData })
  }

  function changeInput(event: ChangeEvent<HTMLInputElement>) {
    const { name, value } = event.target
    setInputData({ ...inputData, [name]: value })
  }

  async function changeForm(event: FormEvent) {
    event.preventDefault()

    const {
      name,
      email,
      username,
      password,
      admin,
      active
    } = inputData

    const data = {
      name,
      email,
      username,
      password,
      admin,
      active
    }

    await api.put(`user/update/${id}`, data, {
      headers: {
        Authorization: `Basic ${getToken()}`
      }
    })

    alert('Usuário Alterado com Sucesso!')

    history.goBack()
  }

  useEffect(() => {
    loadUser(id)
  }, [id])

  async function loadUser(id: string) {
    const response = await api.get(`user/${id}`, {
      headers: {
        Authorization: `Basic ${getToken()}`
      }
    })

    const user = {
      name: response.data.name,
      email: response.data.email,
      username: response.data.username,
      password: response.data.password,
      admin: response.data.admin,
      active: response.data.active
    }

    setInputData(user)
  }

  function cancel() {
    history.goBack()
  }

  return (
    <div id="page-create-user" className="body_form">
      <Form onSubmit={changeForm}>
        <FormGroup>
          <Label className="strong">Nome:</Label>
          <Input
            id="name"
            type="text"
            name="name"
            placeholder="Nome"
            value={inputData.name}
            required
            onChange={changeInput}></Input>

          <Label className="strong">Email:</Label>
          <Input
            id="email"
            type="email"
            name="email"
            placeholder="Email"
            value={inputData.email}
            onChange={changeInput}></Input>

          <Label className="strong">Login:</Label>
          <Input
            id="username"
            type="text"
            name="username"
            placeholder="Login"
            maxLength={11}
            value={inputData.username}
            required
            onChange={changeInput}></Input>

          <Label className="strong">Senha:</Label>
          <Input
            id="password"
            type="password"
            name="password"
            placeholder="Senha"
            value={inputData.password}
            required
            onChange={changeInput}></Input>

          <div className="div_admin">
            <Label className="strong">Admin:</Label>
            <Input
              className="input_check"
              id="admin"
              type="checkbox"
              checked={inputData.admin}
              name="admin"
              onChange={() => changeAdmin(!inputData.admin)}></Input>
          </div>

          <div className="div_admin">
            <Label className="strong">Ativo:</Label>
            <Input
              className="input_check"
              id="ativo"
              type="checkbox"
              checked={inputData.active}
              name="ativo"
              onChange={() => changeActive(!inputData.active)}></Input>
          </div>
        </FormGroup>

        <Button id="cancelar" onClick={cancel}>Cancelar</Button>
        <Button type="submit">Alterar</Button>
      </Form>
    </div>
  )
}
export default EditCategory
