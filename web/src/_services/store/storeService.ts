import api from '../api'
import { getToken } from '../auth'
import Store from '../../models/Store'

export class StoreService {
  // eslint-disable-next-line
  constructor() { }

  async getStores(): Promise<Store[]> {
    try {
      const stores = api.get('stores', {
        headers: {
          Authorization: `Basic ${getToken()}`
        }
      }).then(stores => {
        return stores.data;
      }).catch(() => {
        alert('Nenhum registro encontrado.')
        return [];
      })
      return stores;
    } catch (error) {
      return [];
    }
  }

  async delete(id: number) {
    try {
      await api.delete(`store/delete/${id}`, {
        headers: {
          Authorization: `Basic ${getToken()}`
        }
      }).then(() => {
        alert('Registro removido com sucesso.')
      }).catch(error => {
        alert(error)
      })
    } catch (error) {
      alert('Ocorreu um erro ao remover a loja.')
    }
  }
}

export default StoreService
