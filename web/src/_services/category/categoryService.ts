import api from '../api'
import { getToken } from '../auth'
import Category from '../../models/Category'

export class CategoryService {
  // eslint-disable-next-line
  constructor() { }

  async getCategories(): Promise<Category[]> {
    try {
      const categoies = api.get('categories', {
        headers: {
          Authorization: `Basic ${getToken()}`
        }
      }).then(categoies => {
        return categoies.data;
      }).catch(() => {
        alert('Nenhum registro encontrado.')
        return [];
      })
      return categoies;
    } catch (error) {
      return [];
    }
  }

  async delete(id: number) {
    try {
      await api.delete(`category/delete/${id}`, {
        headers: {
          Authorization: `Basic ${getToken()}`
        }
      }).then(() => {
        alert('Registro removido com sucesso.')
      }).catch(error => {
        alert(error)
      })
    } catch (error) {
      alert('Ocorreu um erro ao remover a categoria.')
    }
  }
}

export default CategoryService
