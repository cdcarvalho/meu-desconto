# Projeto Meu Desconto #


O que é necessário?

* git 2.27.0.windows.1
* nodejs 12.18.0
* PostgreSQL 12.3


Como disponibilizar e rodar a aplicação?

* npm install (server e web)

* criar e popular base:
    Instalar o npx: npm install -g npx
    e
    npx knex:migrate
    npx knex:seed

* Pra subir o server: npm run dev
* Para disponibilziar o web: npm run start

Configurando Sequelize no Projeto Server
* npm install --save sequelize (sequelize@6.3.0)
* npm install --save pg pg-hstore
+ pg-hstore@2.3.3
+ pg@8.3.0

* npm install --save-dev sequelize-cli
+ sequelize-cli@6.2.0


Criando o banco
* npx sequelize db:create

Criando tabela de categoria
* npx sequelize migration:create --name=create-categories

Executar os arquivos da pasta migrationd
* npx sequelize db:migrate
